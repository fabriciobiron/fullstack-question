import React, { Component } from 'react';
import {connect} from 'react-redux'

import Questions from './screens/Questions'
import Introduction from './screens/Introduction'
import Result from './screens/Result'

class QuestionApp extends Component {

  constructor(props) {
    super(props) 
  }

  render(){
    return (
      <div id="question-app">
        { 
          this.props.gameStatus === 'introduction' ? 
            <Introduction /> : 
            (this.props.gameStatus === 'result' ? 
              <Result /> :  
              <Questions />
            ) 
        }
      </div>
    );
  }
  
}

const mapStateToProps = (state) => {
  return {
      gameStatus: state.gameStatus
  }
}

export default connect(mapStateToProps)(QuestionApp);
