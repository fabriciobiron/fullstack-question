import { createStore, combineReducers } from 'redux'
import { questions } from './common/constants'

const listOfQuestions = () => questions

/**
 * Handles list of questions and returns it 
 */
const initialState = {
    questions: listOfQuestions(),
    answers: [],
    questionsLeft: [],
    currentQuestion: {},
    firstQuestion: {},
    result: 0,
    gameStatus: 'introduction'
}


/**
 * Controls states based in dispatched actions
 * @param {object} state 
 * @param {object} action 
 */
function reducer(state = initialState, action) {
    // console.log('Reducer', state, action)

    switch (action.type) {
        case 'START_GAME':
        // Handles 'Start Game' clicking and sets first question to be presented
            let fq = listOfQuestions()
            return Object.assign({}, state, { 
                questionsLeft: state.questions.slice(1,state.questions.length),
                gameStatus: 'questions', 
                currentQuestion: fq[0], 
                firstQuestion: fq[0], 
                questions: state.questions
            })

        case 'SET_ANSWER':
        // Handles user answer: shift questions left and stores answers
            
            let answers = state.answers
            answers.push(action.answer)
            
            const calculatedResult = () => state.result + action.answer.result
            const isThereAnyQuestionLeft = () => state.questionsLeft.length
            
            return Object.assign({}, state, {
                questionsLeft: state.questionsLeft.slice(1,state.questionsLeft.length),
                answers: answers,
                currentQuestion: state.questionsLeft[0],
                result: calculatedResult(),
                gameStatus: isThereAnyQuestionLeft() ? 'questions' : 'result'
            })
        
        case 'PLAY_AGAIN':
        // Handles play again button
            return Object.assign({}, state, { 
                answers: [], 
                currentQuestion: state.firstQuestion, 
                gameStatus: 'introduction', 
                result: 0 
            })
        
        default:
            return state
    }
    
}

const store = createStore(reducer)

export default store