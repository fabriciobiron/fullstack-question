import React, { Component } from 'react';
import QuestionApp from './QuestionApp'
import './App.css'

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="container">
          <div className="row">
            <div className="column"><QuestionApp /></div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
