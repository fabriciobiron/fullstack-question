import React, { Component } from 'react';
import {connect} from 'react-redux'

class QuestionItem extends Component {
  constructor(props) {
    super(props) 
  }

  render(){
    return (
      <div id="question-item" style={{display: (this.props.visibility) ? 'block':'none'}}>
        <h3>{this.props.question.question}</h3>
        <div className="button-group">
        <button className="button" onClick={this.props.setAnswer.bind(this,this.props.question,true)}>True</button>
        <button className="button button-outline" onClick={this.props.setAnswer.bind(this,this.props.question,false)}>False</button>
        </div>
      </div>
    );
  }

}


const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
return {
    setAnswer: (question,answer) => {
      let result = (question.response === answer) ? 1 : 0
      dispatch({type: 'SET_ANSWER', answer: { question , answer, result } })
    }
}
}

export default connect(mapStateToProps,mapDispatchToProps)(QuestionItem);