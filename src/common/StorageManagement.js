

export default new class StorageManagement{

    setRecord(value){
        sessionStorage.setItem('record', value);
    }

    getRecord(){
        return sessionStorage.getItem('record')
    }

    destroyRecord(){
        sessionStorage.removeItem('record');
    }
} 



