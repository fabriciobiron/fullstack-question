export const questions = [
    {
        id: 1,
        category: 'Literature',
        question: "The writer Nancy Huston won the Nobel Prize in Literature in 2013.",
        response: false
    }, {
        id: 2,
        category: 'Sports',
        question: "Clara Hughes won medals at both the Summer and Winter Olympics.",
        response: true
    }, {
        id: 3,
        category: 'Geography',
        question: "The capital of the Yukon is Whitehorse.",
        response: true
    }, {
        id: 4,
        category: 'Geography',
        question: "Canada has 66 million inhabitants.",
        response: false
    }, {
        id: 5,
        category: 'History',
        question: "Julie Payette was the first Canadian astronaut to board the International Space Station.",
        response: true
    }, {
        id: 6,
        category: 'Geography',
        question: "In 1948, Newfoundland initially refused to join Canada.",
        response: true
    }, {
        id: 7,
        category: 'Sports',
        question: "Paul Henderson scored the winning goal in the ice hockey tournament in the 1972 Olympics.",
        response: false
    }, {
        id: 8,
        category: 'History',
        question: "During the Second World War, Canadians defended Hong Kong.",
        response: true
    }, {
        id: 9,
        category: 'Geography',
        question: "Canada is the best destination to see lakes.",
        response: true
    }, {
        id: 10,
        category: 'Geography',
        question: "Canada is bordered by two oceans.",
        response: false
    }
]