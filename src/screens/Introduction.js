import React, { Component } from 'react';
import { connect } from 'react-redux'
import Storage from '../common/StorageManagement'

class Introduction extends Component {

  constructor(props) {
    super(props)
  }

  resetRecord() {
    Storage.destroyRecord()
  }

  render() {
    return (
      <div id="introduction-screen" className="animated fadeIn">
        <h2>Welcome to the Trivia Challenge!</h2>
        <h3>You will be presented with 10 True or False Questions. Can you score 100%?</h3>
          {
            sessionStorage.getItem('record') !== null ?
              (<h4><i className="fas fa-trophy"></i> Your record is {sessionStorage.getItem('record')}</h4>) :
              ('')
          }
        <button className="button" onClick={this.props.startGame.bind()}>
          Let's begin!
        </button>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    startGame: () => {
      dispatch({ type: 'START_GAME' })
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Introduction);