import React, { Component } from 'react';
import { connect } from 'react-redux'

import Storage from '../common/StorageManagement'
class Result extends Component {

  constructor(props) {
    super(props)
    this.state = {
      isItANewRecord: true
    }
  }

  componentWillMount() {
    this.checkRecord(this.props.result)
  }

  /**
   * Renders list of questions and user's responses
   */
  renderDetailedResult() {
    let results = this.props.answers
    let list = results.map((v, index) => {
      return <li key={index}>
        {(v.result) ? <i className="fas fa-plus"></i> : <i className="fas fa-minus"></i>} {v.question.question}
      </li>
    })
    return <ol> {list} </ol>
  }

  /**
   * Verifies if user's points is greater than record
   * @param {number} value 
   */
  checkRecord(value) {
    if (Storage.getRecord() > value) {
      this.setState({ isItANewRecord: false })
    } else {
      this.setState({ isItANewRecord: true })
      Storage.setRecord(value)
    }
  }

  render() {
    return (
      <div id="results-screen" className="animated fadeIn">
        {
          this.state.isItANewRecord ?
            (<h3><i className="fas fa-trophy"></i> You have got a new record!</h3>) :
            (<h3>Keep trying to beat your record which is {sessionStorage.getItem('record')}</h3>)
        }
        <h3>Your score was {this.props.result}/{this.props.answers.length}: </h3>
        <h2>{this.state.message}</h2>
        {this.renderDetailedResult()}
        <button onClick={this.props.dispatchPlayAgain.bind()}>Play Again</button>
      </div>
    );
  }

}

const mapStateToProps = (state) => {
  return {
    answers: state.answers,
    result: state.result
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatchPlayAgain: () => {
      dispatch({ type: 'PLAY_AGAIN' })
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Result);

