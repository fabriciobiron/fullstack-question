import React, { Component } from 'react';
import QuestionItem from '../components/QuestionItem'
import { connect } from 'react-redux'

class Questions extends Component {

  constructor(props) {
    super(props)
  }

  /**
   * Calculates amount of left questions
   */
  countQuestionsLeft() {
    return this.props.questionsLeft.length 
  }

  /**
   * Renders Question information 
   */
  renderQuestion() {
    let questionItem = this.props.currentQuestion
    return (
      <div>
        Category: {questionItem.category}
        <QuestionItem visibility={true} key={questionItem.id} question={questionItem} />
        Questions left: {this.countQuestionsLeft()} <br />
      </div>
    )
  }

  render() {
    return (
      <div id="questions-screen" className="animated fadeInLeft ">
        {this.renderQuestion()}
      </div>
    );
  }
  
}

const mapStateToProps = (state) => {
  return {
    questionsLeft: state.questionsLeft,
    currentQuestion: state.currentQuestion
  }
}

export default connect(mapStateToProps)(Questions);
